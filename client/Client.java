import java.io.*;
import java.net.Socket;


import static java.lang.System.exit;

public class Client
{
	private static String first = null;
	private static String second = null;
	private static String third = null;

	public static void main( String[] args )
	{

		try
		{
			Socket connection = new Socket("localhost", 9999);
			BufferedWriter out = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream()));

			if(args.length == 2) {

				first = args[0];
				second = args[1];
				out.write(first+"\n");
				out.flush();
				out.write(second+"\n");
				out.flush();
				out.write("nodata\n");
				out.flush();

			} else if (args.length == 1) {

				first = args[0];
				out.write(first+"\n");
				out.flush();
				out.write("nodata\n");
				out.flush();
				out.write("nodata\n");
				out.flush();

			} else if (args.length == 0) {

				out.write("nodata\n");
				out.flush();
				out.write("nodata\n");
				out.flush();
				out.write("nodata\n");
				out.flush();

			} else  if (args.length == 3){

				first = args[0];
				second = args[1];
				third = args[2];
				out.write(first+"\n");
				out.flush();
				out.write(second+"\n");
				out.flush();
				out.write(third+"\n");
				out.flush();

			} else {
				System.err.println("Sorry, there are no such option");
				out.write("nodata\n");
				out.flush();
				out.write("nodata\n");
				out.flush();
				out.write("nodata\n");
				out.flush();
			}

			BufferedReader inputStream = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String message;

			//read the answer from server
			while (true) {

				message = inputStream.readLine();
				if(message.equals("close"))
				{
					break;
				}
				System.out.println(message);

			}

			connection.close();
			out.close();
			inputStream.close();
		}
		catch( IOException e )
		{
			e.printStackTrace();
		}

	}
}