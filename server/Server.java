import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;



public class Server
{
	private static String first = null;
	private static String second = null;
	private ServerSocket serverSocket = null;
	private static ExecutorService executorService = null;
	public static ArrayList<ArrayList<String>> acceptNumberOfLists;


	//initialize the array list
	public void createTheAcceptList()
	{
		int one = Integer.parseInt(first);
		int two = Integer.parseInt(second);

	 	acceptNumberOfLists = new ArrayList<>(one);
		for (int i = 0; i < one; ++i)
		{
			acceptNumberOfLists.add(new ArrayList<>(two));

		}
	}

	//set the server and waits for a client
	public void activeServer() {
		executorService = Executors.newFixedThreadPool(25);
			try {
				serverSocket = new ServerSocket(9999);
				while (true) {
					try {
						Socket socket = serverSocket.accept();
						executorService.submit(new RunTask(first, second, socket));
					} catch (IOException ioe) {
						System.out.println("Error accepting connection");
						ioe.printStackTrace();
					}
				}
			} catch (IOException e) {
				System.out.println("Error starting Server on ");
				e.printStackTrace();
			}
		}

	public static void main(String[] args) throws Exception
	{
		if (args.length <= 0) {
			System.err.println("No arguments passed. Please, try again.");
			System.exit(1);
		} else {
			first = args[0];
			second = args[1];
		}

		Server ser = new Server();
		ser.createTheAcceptList();
		ser.activeServer();

	}
}



