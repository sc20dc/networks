import java.io.*;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;


public class RunTask implements Runnable{

    private String first = null;
    private String second = null;
    public Socket socket;
    private BufferedReader input;
    private BufferedWriter out;


    //Create and write the into the log.txt file
    public void logFile(String anyWord) throws IOException {
        File file = new File("log.txt");

        if (file.createNewFile()) {
            System.out.println("File is created.\n");
        } else {
            System.out.println("File is already exist.");
        }
        out.flush();

        PrintWriter writer = new PrintWriter(new FileWriter(file, true));
        writer.write(anyWord+"\n");
        writer.close();
    }


    // gets a string and checks if it is an integer
    public static boolean chekInteger(String Num) {

        if (Num == null) {
            return false;
        }
        try {
            int d = Integer.parseInt(Num);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public RunTask(String numOne, String numTwo, Socket socket) throws IOException
    {
        this.first = numOne;
        this.second = numTwo;
        this.socket = socket;
    }

    public RunTask () {}

    public void runTask() throws IOException
    {
        //format log.txt string output
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy|HH:mm:ss");
        Date date = new Date();
        String formDate = formatter.format(date);
        String ipAddress =  socket.getRemoteSocketAddress().toString().replace("/","");


        boolean isIt = chekInteger(first);
        boolean isIt2 = chekInteger(second);
        int first_ = 0;
        int second_ = 0;

        if(isIt && isIt2){
            first_ = Integer.parseInt(first);
            second_ = Integer.parseInt(second);
        } else {
            out.write("Arguments must be Integers.");
            out.flush();
            System.exit(1);
        }

        String incomingData1 = "";
        String incomingData2 = "";
        String incomingData3 = "";

        while (true) {

            String strForLog = formDate+"|"+ipAddress+"|";
            input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            incomingData1 = input.readLine();
            incomingData2 = input.readLine();
            incomingData3 = input.readLine();

            //write correct logs into the log.txt file
            if (!incomingData1.equals("nodata") && incomingData2.equals("nodata") && incomingData3.equals("nodata"))
            {
               strForLog =  strForLog + incomingData1;
               logFile(strForLog + ".");
            }
            if (!incomingData2.equals("nodata") && !incomingData1.equals("nodata") && incomingData3.equals("nodata"))
            {
                strForLog = strForLog + incomingData1 + " " + incomingData2;
                logFile(strForLog + ".");
            }
            if (!incomingData1.equals("nodata") && !incomingData2.equals("nodata") && !incomingData3.equals("nodata"))
            {
                strForLog = strForLog + incomingData1 + " " + incomingData2 + " " + incomingData3;
                logFile(strForLog + ".");
            }


            // shows total information about the list
            if (incomingData1.equals("totals")) {

                //send general info about lists and their size
                String message = "There are " + first + " list(s), each with a maximum size of " + second + ".\n";
                out.write(message);
                out.flush();

                //send number of members in each list
                for (int i = 1; i < first_ + 1; ++i) {

                    message = "List " + i + " has " + Server.acceptNumberOfLists.get(i - 1).size() + " member(s).\n";
                    out.write(message);
                    out.flush();
                }

            }

            // join a member with restrictions in a number
            if (incomingData1.equals("join") && !incomingData2.equals("nodata")) {

                boolean check = chekInteger(incomingData2);
                if(!check) {
                    out.write("Second argument must be an Integer\n");
                    out.flush();
                    out.write("close\n");
                    out.flush();
                    continue;
                }
                int temp = Integer.parseInt(incomingData2);
                temp = temp - 1;


                if (Server.acceptNumberOfLists.get(temp).size() > second_ - 1)  {
                    out.write("Failed.\n");
                    out.flush();
                    out.write("close\n");
                    out.flush();

                } else {

                    Server.acceptNumberOfLists.get(temp).add(incomingData3);
                }
                out.write("Success.\n");
                out.flush();
                out.write("close\n");
                out.flush();
            }

            //show the names of all members in the list
            if(incomingData1.equals("list") && !incomingData2.equals("nodata")) {



                boolean check = chekInteger(incomingData2);
                if(!check) {
                    out.write("Second argument must be an Integer\n");
                    out.flush();
                    out.write("close\n");
                    out.flush();
                    continue;
                }

                int temp = Integer.parseInt(incomingData2);

                if(temp > Server.acceptNumberOfLists.size() || temp <= 0)
                {
                    out.write("The requested list does not exist!\n");
                    out.flush();
                    out.write("close\n");
                    out.flush();
                    continue;
                }
                String message;

                for(int i = 0; i < Server.acceptNumberOfLists.get(temp-1).size(); ++i) {

                    message = Server.acceptNumberOfLists.get(temp-1).get(i)+"\n";
                    out.write(message);
                    out.flush();

                }

                //send signal to close connection
                message = "close\n";
                out.write(message);
                out.flush();
            }

            out.write("close\n");
            out.flush();

        }

    }

    @Override
    public void run() {
        try {
            runTask();

        } catch (IOException e)
        {
            try {
                out.close();
                input.close();
                socket.close();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
            e.printStackTrace();
        }
    }

}
